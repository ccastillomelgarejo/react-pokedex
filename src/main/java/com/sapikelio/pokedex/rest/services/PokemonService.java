package com.sapikelio.pokedex.rest.services;

import com.sapikelio.pokedex.model.Pokemon;
import com.sapikelio.pokedex.model.dao.PokemonDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PokemonService {

    @Autowired
    PokemonDAO pokemonDAO;


    public List<Pokemon> getPokemons(Integer limit, Integer offset, String name) {
        return pokemonDAO.getPokemons(limit, offset, name);
    }

    public long countPokemons(String name) {
        return pokemonDAO.countPokemons(name);
    }



}
