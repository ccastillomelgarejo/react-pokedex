import React, {Component} from 'react';
import {Link} from 'react-router';
import { Button, Navbar,Glyphicon } from 'react-bootstrap';


export class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="wrapper">
                <nav className="navbar navbar-default"
                     style={{height: "50px" ,"marginBottom":"0px"}}>
                    <div className="container-fluid">
                        <img className="pokeball-logo" width={64} height={64}
                             src={`/images/pokeball.png`}
                             alt="thumbnail"/>
                    </div>
                </nav>
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

