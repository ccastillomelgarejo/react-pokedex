package com.sapikelio.pokedex.model.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.sapikelio.pokedex.model.Pokemon;
import com.sapikelio.pokedex.model.QPokemon;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class PokemonDAO {


    @PersistenceContext
    private EntityManager entityManager;

    QPokemon qPokemon = QPokemon.pokemon;


    public List<Pokemon> getPokemons(Integer limit, Integer offset, String name) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qPokemon);
        if ((name != null) && !name.isEmpty()) {
            query.where(qPokemon.name.containsIgnoreCase(name));
        }

        if (limit != null) {
            query.limit(limit);
        }
        if (offset != null) {
            query.offset(offset);
        }
        return query.list(qPokemon);
    }

    public long countPokemons(String name) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qPokemon);
        if ((name != null) && !name.isEmpty()) {
            query.where(qPokemon.name.containsIgnoreCase(name));
        }
        return query.count();
    }
}
