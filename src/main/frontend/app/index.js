import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Router, Route,Link, IndexRoute,IndexLink, hashHistory} from 'react-router';


//Import custom components
import {PokeList} from './components/poke-list.js';
import {App} from './app.js';


import {NoMatch} from './components/404.js';


//Import main style
import '../style/style.css'


ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute name="Pokemon List" component={PokeList}/>
        </Route>
        <Route name="404: No Match for route" path="*" component={NoMatch}/>
    </Router>,
    document.querySelector('#container')
);

