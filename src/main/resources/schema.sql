CREATE TABLE Pokemon
(
  id    INT(11)      NOT NULL AUTO_INCREMENT,
  name  VARCHAR(255) NOT NULL,
  type1 varchar(255) not null,
  type2 varchar(255) ,
  height FLOAT(11),
  weight FLOAT(11),
  hp INT(11),
  attack INT(11),
  defense INT(11),
  speed INT(11),
  special_attack INT(11),
  special_defense INT(11),
  description varchar(10000) ,
  PRIMARY KEY (id)
);