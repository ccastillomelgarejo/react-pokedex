# React Pokédex

A simple Pokédex built with React and Spring Boot 

[Demo](http://react-dex.herokuapp.com/)

![](https://media.giphy.com/media/yxPqvhOxAvh4MFqV3A/giphy.gif)

## LEGAL

### Pokémon

Pokémon images, names and information (c) 1995-2018 Nintendo/Game freak.

Images and content were taken from the following resources:

* [PokéApi](http://pokeapi.co/)
* [Pokémon Database](http://pokemondb.net/)
