package com.sapikelio.pokedex.rest;


import com.sapikelio.pokedex.model.Pokemon;
import com.sapikelio.pokedex.rest.dto.PaginatedResponse;
import com.sapikelio.pokedex.rest.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class PokemonResource {


    @Autowired
    PokemonService pokemonService;

    @GetMapping("/api/pokemon")
    public PaginatedResponse<Pokemon> getPokemons(
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "name", required = false) String name
    ) {
        return  new PaginatedResponse(
                pokemonService.getPokemons(limit,offset,name),
                pokemonService.countPokemons(name));
    }
}
