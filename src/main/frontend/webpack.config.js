const path = require('path');

module.exports = {
    entry: path.join(__dirname, "app/index.js"),
    output: {
        path: path.join(__dirname, '../../../target/classes/public/static'),
        publicPath: '',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};

