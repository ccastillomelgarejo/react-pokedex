'use strict';

import React, {Component} from 'react';
import {Row, Col, Image, Media, Badge, Modal, Tabs, Tab} from 'react-bootstrap';
import Highcharts from 'highcharts'
import HighchartsMore from 'highcharts/highcharts-more'

HighchartsMore(Highcharts)
import HighchartsReact from 'highcharts-react-official'


function buildChartOptions(data) {
    return {

        chart: {
            polar: true,
            type: 'area'
        },

        title: {
            text: '',
            style: {
                display: 'none'
            }
        },
        credits: {
            enabled: false
        },
        pane: {
            size: '70%'
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                },
                dataLabels: {
                    enabled: true,
                    format: '<span class="wheel-label" style="color: {point.color}">{point.y}</span>',
                }
            }
        },

        xAxis: {
            categories: ['HP', 'Attack', 'Defense', 'Speed', 'SP. Atk',
                'Sp. Def'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0,
            max: 255,
            labels: {
                enabled: false
            }
        },

        tooltip: {
            enabled: false
        },

        legend: {
            enabled: false
        },

        series: [{
            type: 'area',
            name: 'Value',
            color: 'rgb(239, 83, 80)',
            data: data,
            pointPlacement: 'off'
        }]
    }
}

export class PokeListElement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.pokemon.name,
            id: props.pokemon.id,
            description: props.pokemon.description,
            type1: props.pokemon.type1,
            type2: props.pokemon.type2,
            height: props.pokemon.height,
            weight: props.pokemon.weight,
            hp: props.pokemon.hp,
            attack: props.pokemon.attack,
            defense: props.pokemon.defense,
            speed: props.pokemon.speed,
            sp_attack: props.pokemon.specialAttack,
            sp_defense: props.pokemon.specialDefense,
            show: false,
            chart: {}
        }

        this.state.chart = buildChartOptions([
            this.state.hp, this.state.attack,
            this.state.defense, this.state.speed,
            this.state.sp_attack, this.state.sp_defense]);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

    }

    handleClose() {
        this.setState({show: false});
    }

    handleShow() {
        this.setState({show: true});
    }


    componentDidMount() {
    }

    render() {
        return (
            <div>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.id}# {this.state.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="pokemon-body">
                        <Row className="pokemon-body-header">
                            <Col xs={4} className="pokemon-body-col poke-img">
                                <img style={{width: '100%'}}
                                     src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.state.id}.png`}
                                     alt="thumbnail"/>
                            </Col>
                            <Col xs={8}
                                 className="pokemon-body-col poke-basic-data">
                                <Row>
                                    <Col xs={6}>
                                        <b> {this.state.height} m</b>
                                        <br/>
                                        HEIGHT
                                    </Col>
                                    <Col xs={6}>
                                        <b> {this.state.weight} kg</b>
                                        <br/>
                                        WEIGHT
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12}>
                                        <p>
                                            {this.state.description}
                                        </p>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <hr/>
                            <Tabs defaultActiveKey={1}
                                  id="uncontrolled-tab-example">
                                <Tab eventKey={1} title="Stats">
                                    <HighchartsReact
                                        highcharts={Highcharts}
                                        options={this.state.chart}
                                    />
                                </Tab>
                            </Tabs>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Col className="poke-element ripple" sm={12} md={6} mdOffset={3}
                     onClick={this.handleShow}>
                    <Media>
                        <Media.Left>
                            <img width={64} height={64}
                                 src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.state.id}.png`}
                                 devby="spklio"
                                 alt="thumbnail"/>
                        </Media.Left>
                        <Media.Body>
                            <Media.Heading>{this.state.id}# {this.state.name}</Media.Heading>
                            <p>
                                <Badge
                                    className={this.state.type1}>{this.state.type1}</Badge>
                                {this.state.type2 ?
                                    (<Badge
                                        className={this.state.type2}>{this.state.type2}</Badge>) : ("")}
                            </p>
                        </Media.Body>
                    </Media>
                    <hr/>
                </Col>
            </div>
        );
    }
}
