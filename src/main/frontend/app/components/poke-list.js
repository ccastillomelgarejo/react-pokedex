'use strict';

import React, {Component} from 'react';
import {
    Row,
    Form,
    Col,
    FormGroup,
    FormControl,
    Button,
    InputGroup
} from 'react-bootstrap';
import {PokeListElement} from './poke-list-element';
import axios from 'axios';


const PAGE_SIZE = 20;

export class PokeList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
            total: 0,
            isLoading: false,
            name: ""
        };

        this.onScroll = this.onScroll.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
        this.onButtonPressed = this.onButtonPressed.bind(this);
        this.getListReference = this.getListReference.bind(this);
    }


    componentDidMount() {
        this.onSearch(PAGE_SIZE, 0, this.state.name);
        document.getElementsByClassName("poke-list")[0].addEventListener('scroll', this.onScroll, false)
    }

    componentWillUnmount() {
        document.getElementsByClassName("poke-list")[0].removeEventListener('scroll', this.onScroll, false);
    }

    onSearch(limit, offset, name) {
        this.setState({
            isLoading: true
        });

        axios.get("/api/pokemon?limit=" + limit + "&offset=" + offset + "&name=" + name)
            .then(response =>
                this.setState({
                    pokemons: this.state.pokemons.concat(response.data.data),
                    total: response.data.total,
                    isLoading: false
                })
            );
    }

    onScroll() {
        if ((document.getElementsByClassName("poke-list")[0].offsetHeight
            + document.getElementsByClassName("poke-list")[0].scrollTop)
            >= (document.getElementsByClassName("poke-list")[0].scrollHeight - 100) &&
            this.state.pokemons.length < this.state.total
            && !this.state.isLoading) {

            this.onSearch(PAGE_SIZE, this.state.pokemons.length, this.state.name);
        }
    }

    getListReference(element) {
        this.setState({listElem: element});

    }

    onChangeFilter(e) {
        this.setState({name: e.target.value});
    }

    onButtonPressed(e) {
        this.setState({
            pokemons: [],
            total: 0
        });
        this.onSearch(PAGE_SIZE, 0, this.state.name);
    }

    render() {
        return (
            <div>
                <Row className="poke-filter">
                    <Col sm={12} md={6} mdOffset={3}>
                        <Form>
                            <FormGroup controlId="name">
                                <InputGroup>
                                    <FormControl type="text"
                                                 onChange={this.onChangeFilter}/>
                                    <InputGroup.Button>
                                        <button className="btn"
                                            onClick={this.onButtonPressed}>Search</button>
                                    </InputGroup.Button>
                                </InputGroup>
                            </FormGroup>{' '}

                        </Form>
                        <hr/>
                    </Col>
                </Row>
                <Row className="poke-list">
                    {this.state.pokemons.map((poke, idx) =>
                        <PokeListElement pokemon={poke} key={`poke-${idx}`}/>
                    )}
                </Row>
            </div>
        );
    }
}
